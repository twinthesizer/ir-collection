#include <Arduino.h>
#include <IRremote.h>

// Which kind of LED's can I use

// When Pendulum sensor catches light,
// measure duration between last detection and current detection
// to get both time and position (running phase)

// Defining LED pin as output
const int RECV_PIN0 = 0;
const int RECV_PIN1 = 1;
const int RECV_PIN2 = 2;
const int RECV_PIN3 = 3;

IRrecv irrecv0(RECV_PIN0);
IRrecv irrecv1(RECV_PIN1);
IRrecv irrecv2(RECV_PIN2);
IRrecv irrecv3(RECV_PIN3);
decode_results results;

// Where are we reading the data into
// Where will we process the incoming data

// Setup a timer after first LED detection until last. when long pause finish cycle, restart

void setup()
{
    Serial.begin(9600);
    irrecv0.enableIRIn();
    irrecv0.blink13(true);

    irrecv1.enableIRIn();
    irrecv1.blink13(true);

    irrecv2.enableIRIn();
    irrecv2.blink13(true);

    irrecv3.enableIRIn();
    irrecv3.blink13(true);
}

void loop()
{
    if (irrecv0.decode(&results)) {
        Serial.println(results.value, HEX);
        irrecv0.resume();
    }
}

// If count++ toggle IR LED on and off
// Datasheet of specific IR emitter

// Different setting for LED (frequency change) depending on environment when button is pressed

// hexi to whatever
// read values and then compare
